"""
Application config that uses environs module to load values from .env file or environment variables
"""

import logging
from environs import Env

logger = logging.getLogger(__name__)

env = Env()
env.read_env()

class Config:
    TITLE = 'AGR Orthology Normalizer'
    VERSION = '0.0.1'
    DESCRIPTION = 'An application to aid in normalizing AGR orthology data.'

    SECRET_KEY = env.str('SECRET_KEY')

    DEBUG = env.bool('DEBUG', default=False)
    TESTING = env.bool('TESTING', default=False)
    LOG_LEVEL = env.str("LOG_LEVEL", default="WARNING")

    # TODO - update to point to relevant databases
    DATABASE_URL = 'postgresql://user:pass@localhost:5432/agr'
