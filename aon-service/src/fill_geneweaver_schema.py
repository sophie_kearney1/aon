from models import Geneweaver_Gene, Geneweaver_Species, Geneweaver_GeneDB
from csv import reader
from database import SessionLocal

db = SessionLocal()

# TODO - update file paths
gene_file_path = 'aon/aon-service/geneweaver_schema_data/geneweaver_gene.csv'
genedb_file_path = 'aon/aon-service/geneweaver_schema_data/geneweaver_genedb.csv'
species_file_path = 'aon/aon-service/geneweaver_schema_data/geneweaver_species.csv'


def add_geneweaver_species(file_path):
    '''
        :param: file_path - file path to file geneweaver_species.csv that contains all species
                to be added to the database
        :description: adds species from the geneweaver database into the species table of the geneweaver schema
    '''
    with open(file_path, 'r') as f:
        csv_reader = reader(f)
        next(csv_reader)
        species_file_list = list(csv_reader)

    species_objects = []
    for s in species_file_list:
        species_object = Geneweaver_Species(sp_id=int(s[0]), sp_name=s[1], sp_taxid=int(s[2]),
                                            sp_ref_gdb_id=s[3], sp_date=s[4], sp_biomart_info=s[5],
                                            sp_source_data=s[6])
        species_objects.append(species_object)
    db.bulk_save_objects(species_objects)
    db.commit()


def add_geneweaver_genedb(file_path):
    '''
        :param: file_path - file path to file geneweaver_genedb.csv that contains all genedb info
                to be added to the database
        :description: adds genedb from the geneweaver database into the genedb table of the geneweaver schema
    '''
    with open(file_path, 'r') as f:
        csv_reader = reader(f)
        next(csv_reader)
        genedb_file_list = list(csv_reader)

    genedb_objects = []
    for g in genedb_file_list:
        genedb = Geneweaver_GeneDB(gdb_id=int(g[0]), gdb_name=g[1], sp_id=int(g[2]),
                               gdb_shortname=g[3], gdb_date=g[4], gdb_precision=int(g[5]),
                               gdb_linkout_url=g[6])
        genedb_objects.append(genedb)
    db.bulk_save_objects(genedb_objects)
    db.commit()


def add_geneweaver_genes(file_path):
    '''
        :param: file_path - file path to file geneweaver_gene.csv that contains all genes
                to be added to the database
        :description: adds genes from the geneweaver database into the gene table of the geneweaver schema
    '''
    with open(file_path, 'r') as f:
        csv_reader = reader(f)
        next(csv_reader)
        gene_file_list = list(csv_reader)

    gene_objects = []
    for g in gene_file_list:
        old_ode_gene_ids = 0
        if g[6] != 'NULL':
            old_ode_gene_ids = g[6]
        gene = Geneweaver_Gene(ode_gene_id=int(g[0]), ode_ref_id=g[1], gdb_id=int(g[2]),
                               sp_id=int(g[3]), ode_pref=bool(g[4]), ode_date=g[5],
                               old_ode_gene_ids=old_ode_gene_ids)
        gene_objects.append(gene)
    db.bulk_save_objects(gene_objects)
    db.commit()


if __name__ == '__main__':
    add_geneweaver_species(species_file_path)
    add_geneweaver_genedb(genedb_file_path)
    add_geneweaver_genes(gene_file_path)
